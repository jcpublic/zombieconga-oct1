//
//  GameScene.swift
//  ZombieConga
//
//  Created by Parrot on 2019-01-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
   
    // MARK: Sprites
    // -------------------------
    var zombie:SKSpriteNode!
    var grandma:SKSpriteNode!
    var livesLabel:SKLabelNode!
    var scoreLabel:SKLabelNode!
    
    var lives = 100
    var score = 0
    
    var mouseX:CGFloat = 0
    var mouseY:CGFloat = 0
    
    
    override func didMove(to view: SKView) {
        // Set the background color of the app
        self.backgroundColor = SKColor.black;
        
        // Make a background
        let background = SKSpriteNode(imageNamed:"background1")
        background.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        addChild(background)
        
        
        
        
        // Add your zombie
        self.zombie = SKSpriteNode(imageNamed: "zombie1")
        self.zombie.position = CGPoint(x: 400, y: 400)
        addChild(zombie)
        
        // Add you gramma
        self.grandma = SKSpriteNode(imageNamed: "enemy")
        self.grandma.position = CGPoint(x:size.width - 100, y:size.height / 2)
        addChild(self.grandma)
        
        // Add life label
        self.livesLabel = SKLabelNode(text: "Lives Remaining: \(lives)")
        self.livesLabel.position = CGPoint(x:400, y:800)
        self.livesLabel.fontColor = UIColor.magenta
        self.livesLabel.fontSize = 65
        self.livesLabel.fontName = "Avenir"
        addChild(self.livesLabel)
        
        
        // Add a score label
        self.scoreLabel = SKLabelNode(text: "Score: \(self.score)")
        self.scoreLabel.position = CGPoint(x:400, y:1000)
        self.scoreLabel.fontColor = UIColor.magenta
        self.scoreLabel.fontSize = 65
        self.scoreLabel.fontName = "Avenir"
        addChild(self.scoreLabel)
        
        
        
        // Make gramma move using automatic movement
        let move1 = SKAction.move(to: CGPoint(x: size.width/2 , y: 400),
                                  duration: 2)
        let move2 = SKAction.move(to:CGPoint(x:100, y:size.height/2), duration:2)
        let move3 = SKAction.move(to:CGPoint(x:size.width/2, y:400), duration:2)
        let move4 = SKAction.move(to:CGPoint(x:size.width - 100, y:size.height / 2), duration:2)
        
        let grandmaAnimation = SKAction.sequence(
            [move1,move2, move3, move4]
        )
//        let zombieAnimation = SKAction.sequence(
//            [move3, move4]
//        )
        
        // make gramma move in this pattern forever
        let grandmaForeverAnimation = SKAction.repeatForever(grandmaAnimation)
        self.grandma.run(grandmaForeverAnimation)

        //self.zombie.run(zombieAnimation)
        
    }
    
    
    var cats:[SKSpriteNode] = []
    
    func spawnCat() {
        // Add a cat to a static location
        let cat = SKSpriteNode(imageNamed: "cat")
        
        // generate a random x position
        
        let randomXPos = CGFloat.random(in: 0 ... size.width)
        let randomYPos = CGFloat.random(in: 0 ... size.height)
        cat.position = CGPoint(x:randomXPos, y:randomYPos)
        
        // add the cat to the screen
        addChild(cat)
        
        // add the cat to the array
        self.cats.append(cat)
        
    }
   

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // 1. @TODO: write the code to get the mouse location
        let locationTouched = touches.first
        if (locationTouched == nil) {
            // an error occured
            return
        }
        
        let mousePosition = locationTouched!.location(in:self)
        
        print("mouseX = \(mousePosition.x)")
        print("mouseY = \(mousePosition.y)")
        print("-------")
        
        
        // 2. @TODO: set the global mosueX and mouseY variables to the mouse (x,y)
        self.mouseX = mousePosition.x
        self.mouseY = mousePosition.y
    }
    
    func moveZombie(mouseXPosition:CGFloat, mouseYPostion:CGFloat) {
        
        // move the zombie towards the mouse
        // @TODO: Get the android code for moving bullet towards enemey
        // Implement the algorithm in Swift
        
        // 1. calculate disatnce between mouse and zombie
        let a = (self.mouseX - self.zombie.position.x);
        let b = (self.mouseY - self.zombie.position.y);
        let distance = sqrt((a * a) + (b * b))
        
        // 2. calculate the "rate" to move
        let xn = (a / distance)
        let yn = (b / distance)
        
        // 3. move the bullet
        self.zombie.position.x = self.zombie.position.x + (xn * 10);
        self.zombie.position.y = self.zombie.position.y + (yn * 10);
        
    }
    
    // SAME AS UPDATEPOSITIONS
    
    var numLoops = 0
    
    override func update(_ currentTime: TimeInterval) {
        
        // OPTION 1 for generating 1 cat every 2 seconds
        // -- Tie the cat spawn rate to the frame rate
        // -- The game loops 60 times per second (fps = 60)
        // -- Therefore, it will loop 120 times in 2 seconds
        // -- Detect when the the loop has run 120 times
        // -- When count = 120, spawn a cat
        numLoops = numLoops + 1
        if (numLoops % 120 == 0) {
            // make a cat
            self.spawnCat()

        }

        
        // move the zomibe
        self.moveZombie(mouseXPosition: self.mouseX, mouseYPostion: self.mouseY)
        
        // Detect when zombie and gramma collide
        if (self.zombie.frame.intersects(self.grandma.frame) == true) {
            print("\(currentTime): COLLISON!")
            self.lives = self.lives - 1
            
            // update the life counter
            self.livesLabel.text = "Lives Remaining: \(lives)"
            
            // SHOW LOSE SCREEN
            let loseScene = LoseScreen(size: self.size)
            //let transitionEffect = SKTransition.flipHorizontal(withDuration: 2)
            self.view?.presentScene(loseScene)
            //self.view?.presentScene(loseScene, transition:transitionEffect)
            
            
            
        }
        
        
        // detect collisions between zombie and cats
        
        // loop through your array of cats
        // if the player is touching the cat, then remove the cat & increase the score
        for (index, cat) in self.cats.enumerated() {
            if (self.zombie.frame.intersects(cat.frame) == true) {
                // increase teh score & update the label
                self.score = self.score + 1
                self.scoreLabel.text = "Score: \(self.score)"
                // remove cat from screen
                cat.removeFromParent()
                // remove cat from array
                self.cats.remove(at:index)
            }
        }
        
        
        
        
        
        
    }
    
}
