//
//  WinScene.swift
//  ZombieConga
//
//  Created by MacStudent on 2019-10-01.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import SpriteKit

class WinScene:SKScene {
    // constructor
    override init(size: CGSize) {
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // @TODO:
    // 1. Setup the win screen with correct background image
    // 2. Write game logic to show the win screen when score > 3
    // 3. When person touches the win screen, go back to game
}
